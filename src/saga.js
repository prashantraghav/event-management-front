import { call, takeEvery, takeLatest, put, all } from "redux-saga/effects"
const BASE_URL = 'http://localhost:3000';

function* fetchAllEvents() {
    try {
        const api = yield call(fetch, `${BASE_URL}/events/`, { mode: 'cors' });
        const data = yield api.json();
        yield put({ type: 'FETCH_ALL', payload: { events: data } })
    } catch (err) {
        console.log('error', err);
        yield put({ type: 'ERROR', payload: { error: err } })
    }
}

function* fetchEventDetails(params) {
    try {
        const api = yield call(fetch, `${BASE_URL}/events/${params.payload.event_id}`, { mode: 'cors' });
        const data = yield api.json();
        yield put({ type: 'FETCH_EVENT_DETAILS', payload: { event: data } })
    } catch (err) {
        console.log('error', err);
        yield put({ type: 'ERROR', payload: { error: err } })
    }
}

function* fetchEventsByName(params) {
    try {
        const api = yield call(fetch, `${BASE_URL}/events/search?name=${params.payload.event_name}`, { mode: 'cors' });
        const data = yield api.json();
        yield put({ type: 'FETCH_ALL', payload: { events: data } })
    } catch (err) {
        console.log('error', err);
        yield put({ type: 'ERROR', payload: { error: err } })
    }
}

function* fetchAttendeesDetails(params) {
    try {
        const api = yield call(fetch, `${BASE_URL}/events/${params.payload.event_published_url}/attendees`, { mode: 'cors' });
        const data = yield api.json();
        yield put({ type: 'FETCH_EVENT_ATTENDEES_DETAILS', payload: { attendees: data } })
    } catch (err) {
        console.log('error', err);
        yield put({ type: 'ERROR', payload: { error: err } })
    }
}

function* createNewEvent(params) {
    try {
        const api = yield call(fetch, `${BASE_URL}/events/`,
            {
                method: 'POST',
                mode: 'cors',
                body: JSON.stringify(params.payload.event),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }
        );
        yield api.json();
        yield fetchAllEvents();
    } catch (err) {
        console.log('error', err);
        yield put({ type: 'ERROR', payload: { error: err } })
    }
}

function* updateEvent(params){
    try {
        const api = yield call(fetch, `${BASE_URL}/events/${params.payload.event_id}`,
            {
                method: 'POST',
                mode: 'cors',
                body: JSON.stringify(params.payload.event),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }
        );
        const data = yield api.json();
        yield put({ type: 'UPDATED_NEW_EVENT', payload: { response: data } })
    } catch (err) {
        console.log('error', err);
        yield put({ type: 'ERROR', payload: { error: err } })
    }
}

function* publishEvent(params){
    try {
        const api = yield call(fetch, `${BASE_URL}/events/${params.payload.event_id}/publish`,
            {
                method: 'PUT',
                mode: 'cors',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }
        );
        yield api.json();
        yield fetchAllEvents();
    } catch (err) {
        console.log('error', err);
        yield put({ type: 'ERROR', payload: { error: err } })
    }
}

function* deleteEvent(params) {
    try {
        const api = yield call(fetch, `${BASE_URL}/events/${params.payload.event_id}`,
            {
                method: 'DELETE',
                mode: 'cors',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }
        );
        yield api.json();
        yield fetchAllEvents();
    } catch (err) {
        console.log('error', err);
        yield put({ type: 'ERROR', payload: { error: err } })
    }
}

function* addAttendee(params) {
    try {
        const api = yield call(fetch, `${BASE_URL}/events/${params.payload.event.published_url}/attendees`,
            {
                method: 'POST',
                mode: 'cors',
                body: JSON.stringify(params.payload.attendee),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }
        );
        yield api.json();
        yield fetchAttendeesDetails({payload: {event_published_url: params.payload.event.published_url}});
    } catch (err) {
        console.log('error', err);
        yield put({ type: 'ERROR', payload: { error: err } })
    }
}

export default function* rootSaga() {
    yield all([
        yield takeEvery('FETCH_ALL_ASYNC', fetchAllEvents),
        yield takeEvery('FETCH_EVENT_DETAILS_ASYNC', fetchEventDetails),
        yield takeEvery('FETCH_EVENT_ATTENDEES_ASYNC', fetchAttendeesDetails),
        yield takeLatest('CREATE_NEW_EVENT_ASYNC', createNewEvent),
        yield takeLatest('UPDATE_EVENT_ASYNC', updateEvent),
        yield takeLatest('ADD_ATTENDEE_ASYNC', addAttendee),
        yield takeLatest('PUBLISH_ASYNC', publishEvent),
        yield takeLatest('SEARCH_EVENT_ASYNC', fetchEventsByName),
        yield takeLatest('DELETE_EVENT_ASYNC', deleteEvent)
    ])
}

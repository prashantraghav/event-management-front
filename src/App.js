import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import EventsConnector from './connectors/events-connector';
import EventAttendeeConnector from './connectors/event-attendees-connector';
import EventFormConnector from './connectors/event-form-connector';
import Navbar from './components/navbar';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar />
          <div className="container">
          <Switch>
            <Route path='/' exact component={EventsConnector}/>
            <Route path='/events' exact component={EventsConnector}/>
            <Route path='/events/new' exact component={EventFormConnector}/>
            <Route path='/events/:id/edit' exact component={EventFormConnector}/>
            <Route path='/events/:id/attendees' exact component={EventAttendeeConnector}/>

          </Switch>
          </div>
        
        </div>
      </BrowserRouter>
    );
  }
}

export default App;

import React from 'react';

const onSubmit = (props) => {
    const attendee = {
        name: document.getElementById('name').value,
        contact: document.getElementById('contact').value,
        email: document.getElementById('email').value,
    }

    props.onSubmit(props.event, attendee);
}

const AttendeeForm = (props) => {
    return (
        <div className="attendee-form">
            <div className="page-header"><h1>Add Attendee</h1></div>
            <form className="event-form horizontal-form" >
                <div className="form-group">
                    <input className="form-control" type="text" id="name" placeholder="Name"></input>
                </div>
                <div className="form-group">
                    <input className="form-control" type="text" id="contact" placeholder="contact"></input>
                </div>
                <div className="form-group">
                    <input className="form-control" type="email" id="email" placeholder="email"></input>
                </div>
                <button type="button" className="btn btn-primary" onClick={() => onSubmit(props)}> Submit </button>
            </form>
        </div>
    )
}

export default AttendeeForm;
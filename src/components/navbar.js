import React from 'react';
import { Link } from 'react-router-dom';


const Navbar = (props) => {
    return (
        <nav className="navbar navbar-inverse">
            <div className="container-fluid">
                <div className="navbar-header">
                    <Link className="navbar-brand" to="/">Events</Link>
                </div>
                <div id="navbar" className="navbar-collapse collapse">
                    <ul className="nav navbar-nav">
                        <li><Link to="/events/new">new events</Link> </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;
import React from 'react';

const Attendee = (props) => {
    return (
        <tr className="attendee">
            <td className="name"> {props.name} </td>
            <td className="contact"> { props.contact} </td>
            <td className="email"> {props.email} </td>
        </tr>
    )
}

export default Attendee;
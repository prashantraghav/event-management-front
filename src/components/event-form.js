import React from 'react';

const onSubmit = (props) => {
    const event = {
        name: document.getElementById('name').value,
        details: document.getElementById('details').value,
        venue: document.getElementById('venue').value,
        dateTime: document.getElementById('dateTime').value
    }
    
    props.onSubmit(event);
    clearAll(props);
}

const clearAll = (props) => {
    document.getElementById('name').value = document.getElementById('details').value =  document.getElementById('venue').value = document.getElementById('dateTime').value = ''
}

const EventForm = (props) => {
    return (
        <div>
            <div class="page-header"><h1> Create New Event</h1></div>
            <form className="event-form horizontal-form" >
                <div className="form-group">
                    <input className="form-control" type="text" id="name" placeholder="Name" />
                </div>
                <div className="form-group">
                    <input className="form-control" type="text" id="details" placeholder="Details" />
                </div>
                <div className="form-group">
                    <input className="form-control" type="text" id="venue" placeholder="Venue"  />
                </div>
                <div className="form-group">
                    <input className="form-control" type="date" id="dateTime" placeholder="Date" />
                </div>
                <button type="button" className="btn btn-primary" onClick={() => onSubmit(props)}> Submit </button>
            </form>
        </div>
    )
}


export default EventForm;
import React from 'react';
import { Link } from 'react-router-dom';

const Event = (props) => {
    
    const renderDetailsView = () => {
        return (
            <div className="row">
                <div className="col-xs-12">
                    <div className="jumbotron">
                        <h1>{props.name}</h1>
                        <p>{props.details}</p>
                        <div>{props.dateTime } @ {props.venue}</div>
                    </div>
                </div>
            </div>
        )
    }
    const renderTableRowView = () => {
        return (
            <tr className="event">
                <td className="name"> {props.name} {props.published_url ? <i className="fas fa-caret-up"></i> : null } </td>
                <td className="details"> {props.details} </td>
                <td className="data-time-and-venue"> {props.dateTime} @ {props.venue} </td>
                <td> 
                    <Link className="link" to={`/events/${props._id}/edit`} title='Edit'><i className='fas fa-pen'></i></Link>
                    {props.published && <Link className="link" to={`/events/${props._id}/attendees`} title='Attendees'><i className='fas fa-users'></i></Link>}
                    {!props.published && <Link className="link" to={`#`} title='Publish' onClick={() => props.publish(props._id)}><i className='fas fa-cloud-upload-alt'></i></Link>}
                    <Link className="link" to={`#`} title="Delete" onClick={() => props.delete(props._id)}> <i className="fa fa-trash"></i></Link>
                </td>
            </tr>
        )
    }

    return (
        (props.detailsView) ? renderDetailsView() : renderTableRowView()
    )
}

export default Event;
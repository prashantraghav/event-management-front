import {createStore, applyMiddleware} from 'redux';
import {devToolsEnhancer} from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga';

import eventsReducer from '../reducers/events-reducer';
import rootSaga from '../saga';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(eventsReducer, devToolsEnhancer(), applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);


export default store;  
import {connect} from 'react-redux';
import EventFormContainer from '../containers/event-form-container';

const mapStateToProps = (state, ownPorps) => ({
    event: state.event
})

const mapDispatchToProps = (dispatch) => {
    return {
        create: (event) => dispatch({type: 'CREATE_NEW_EVENT_ASYNC', payload: {event: event}}),
        update: (event) => dispatch({type: 'UPDATE_EVENT_ASYNC', payload: {event: event}}),
        fetchEventDetails: (event_id) => dispatch({type: 'FETCH_EVENT_DETAILS_ASYNC', payload: {event_id: event_id}}),
    }
}

const EventFormConnector = connect(mapStateToProps, mapDispatchToProps)(EventFormContainer);

export default EventFormConnector;

import {connect} from 'react-redux';
import EventsContainer from '../containers/events-container';

const mapStateToProps = (state, ownPorps) => ({
    events: state.events,
    event: state.event
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllEvents: () => dispatch({type: 'FETCH_ALL_ASYNC'}),
        publish: (event_id) => dispatch({type: 'PUBLISH_ASYNC', payload: {event_id: event_id}}),
        search: (name) => dispatch({type: 'SEARCH_EVENT_ASYNC', payload: {event_name: name}}),
        delete: (event_id) => dispatch({type: 'DELETE_EVENT_ASYNC', payload: {event_id: event_id}})
    }
}

const EventsConnector = connect(mapStateToProps, mapDispatchToProps)(EventsContainer);

export default EventsConnector;

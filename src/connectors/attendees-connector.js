import {connect} from 'react-redux';
import AttendeesContainer from '../containers/attendees-container';

const mapStateToProps = (state, ownPorps) => ({
    attendees: state.attendees,
    event: ownPorps.event
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAttendees: (url) => dispatch ({type: 'FETCH_EVENT_ATTENDEES_ASYNC', payload: {event_published_url: url}}),
        addAttendee: (event, attendee) => dispatch({type: 'ADD_ATTENDEE_ASYNC', payload: {event: event, attendee: attendee}})
    }
}

const AttendeesContector = connect(mapStateToProps, mapDispatchToProps)(AttendeesContainer);

export default AttendeesContector;

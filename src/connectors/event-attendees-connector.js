import {connect} from 'react-redux';
import EventAttendeesContainer from '../containers/event-attendees-container';

const mapStateToProps = (state, ownPorps) => ({
    event: state.event
})

const mapDispatchToProps = (dispatch) => {
    return {
        fetchEventDetails: (event_id) => dispatch({type: 'FETCH_EVENT_DETAILS_ASYNC', payload: {event_id: event_id}}),
        //publishEvent: (_id) => dispatch({type: 'PUBLISH', payload: {id: _id}})
    }
}

const EventAttendeeConnector = connect(mapStateToProps, mapDispatchToProps)(EventAttendeesContainer);

export default EventAttendeeConnector;

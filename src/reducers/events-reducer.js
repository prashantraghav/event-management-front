
const createdNewEvent = (state, payload) => {
    state.events.push(payload.event);
    return state.events;
}

const eventsReducer = (state = {events: []}, {type, payload}) => {
    const currentState = JSON.parse(JSON.stringify(state));
    switch(type){
        case 'FETCH_ALL':
            return {...currentState, events: payload.events}
        case 'FETCH_EVENT_DETAILS':
            return {...currentState, event: payload.event}
        case 'FETCH_EVENT_ATTENDEES_DETAILS':
            return {...currentState, event: currentState.event, attendees: payload.attendees}
        case 'CREATED_NEW_EVENT':
            return {...currentState, events: createdNewEvent(currentState, payload)}
        case 'PUBLISH':
            return {...currentState, events: currentState.events}
        default:
            return currentState;
    }
}

export default eventsReducer;
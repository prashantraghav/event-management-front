import React, {Component} from 'react';

import Event from '../components/event';
import AttendeesContector from '../connectors/attendees-connector';


class EventAttendeesContainer extends Component {
    
    componentDidMount () {
        this.props.fetchEventDetails(this.props.match.params.id);
    }

    render(){
        return (
            <div className="event-attendees-container">
                {this.props.event && <Event {...this.props.event} detailsView={true} /> }
                {this.props.event && <AttendeesContector event={this.props.event} />}
            </div>
        )
    }
}

export default EventAttendeesContainer;
import React, { Component } from 'react'
import Event from '../components/event';

class EventsContainer extends Component {

    componentDidMount() {
        this.props.fetchAllEvents();
    }

    render() {
        return (
            <div className="event-list">
                <div class="page-header"><h1>Event List</h1></div>
                <input type="text" name="search" className="form-group" onChange={(e)=> this.props.search(e.target.value)} placeholder="search"/>
                <table className="events table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Details</th>
                            <th>Venue</th>
                            <th width="150px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.events && this.props.events.map((event) => { return <Event {...event} publish={this.props.publish} delete={this.props.delete}/> })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default EventsContainer;
import React, { Component } from 'react';
import Attendee from '../components/attendee';
import AttendeeForm from '../components/attendee-form';

class AttendeesContainer extends Component {

    componentDidMount() {
        this.props.fetchAttendees(this.props.event.published_url)
    }

    render() {
        return (
            <div className="row">
                <div className="col-xs-8">
                    <div className="page-header"><h1>Attendees</h1></div>
                    <table className="AttendeesContainer table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Contact</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.attendees && this.props.attendees.map((attendee) => <Attendee {...attendee} />)}
                        </tbody>
                    </table>
                </div>
                <div className="col-xs-4">
                    <AttendeeForm event={this.props.event} onSubmit={this.props.addAttendee}/>
                </div>
            </div>
        )
    }
}

export default AttendeesContainer;
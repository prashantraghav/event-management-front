import React, { Component } from 'react';
import EventForm from '../components/event-form';
import EventsConnector from '../connectors/events-connector';


class EventFormContainer extends Component {
    render() {
        return (
            <div className="event-form-container">
                <div className="row">
                    <div className="col-xs-8">
                        <EventsConnector />
                    </div>
                    <div className="col-xs-4">
                        <EventForm onSubmit={this.props.create} />
                    </div>
                </div>
            </div>
        )
    }
}

export default EventFormContainer;